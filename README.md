Masa OpenMP
=========

The ansible role Masa OpenMP downloads, compile and install MASA OpenMP.

Requirements
------------

It is required to have Ansible (version 2.1 or above) and root access

Role Variables
--------------

Ansible variables and default values (see `defaults/main.yml` and `vars/main.yml`).


    masa_repo_url: 'https://github.com/edanssandes/masa-openmp'

Masa's repository URL.

    masa_version: '1.0.1.1024'

Masa version

    arch: 'intel'

Target architecture to be used to compile MASA. Either 'intel' or 'arm'.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: instances
      roles:
         - { role: diegomachadosoares.masa-openmp }

License
-------

MIT / BSD

Author Information
------------------

This role was created by Diego M. Soares
